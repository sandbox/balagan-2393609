<?php

/**
 * Implements hook_field_widget_info().
 */
function exif_date_field_widget_info() {
  return array(
    'exif_date' => array(
      'label' => t('Read date from exif data'),
      'field types' => array('datestamp'), // 'date', 'datetime'),  // date is the Date (ISO format)
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
    ),
  );
}

/**
 * Implements field_widget_settings_form().
 */
function exif_date_field_widget_settings_form($this_field, $instance) {
  $settings = $instance['widget']['settings'];

  $entity_fields = field_info_instances($instance['entity_type'], $instance['bundle']);
  $all_fields = field_info_fields();
  $supported_field_types = array('file', 'image'); //exif_date_supported_field_types();

  $field_types = array();
  $valid_fields = array();

  // Add in the title/name
  //@@TODO Do this programatically by getting entity_info
  switch ($instance['entity_type']) {
    case 'node':
      $all_fields['title'] = array(
        'field_name' => 'title',
        'type' => 'text',
      );

      $entity_fields['title']['label'] = t('Title');
      break;

    case 'taxonomy_term':
      $all_fields['name'] = array(
        'field_name' => 'name',
        'type' => 'text',
      );

      $entity_fields['name']['label'] = t('Name');
      break;
  }

  // Get a list of all valid fields that we both support and are part of this entity
  foreach ($all_fields as $field) {
    if (array_key_exists($field['field_name'], $entity_fields)) {
      if (in_array($field['type'], $supported_field_types) && ($field['field_name'] != $this_field['field_name'])) {
        $valid_fields[$field['field_name']] = $entity_fields[$field['field_name']]['label'];
         // $available_handlers[$handler] = $processors[$handler]['title'];
         // $handlers_by_type[$field['type']][] = $handler;
        $field_types[$field['field_name']] = $field['type'];
      }
    }
  }
  $form['exif_date_field'] = array(
    '#type' => 'select',
    '#title' => t('Read date from exif'),
    '#default_value' => isset($settings['exif_date_field']) ? $settings['exif_date_field']: '',
    '#options' => $valid_fields,
    '#description' => t('Select which field you would like to read the exif date.'),
    '#required' => TRUE,
  );
/*
  $form['handler_settings'] = array(
    '#tree' => TRUE,
  );

  // Add the handler settings forms
  /*
  foreach ($processors as $handler_id => $handler) {
    if (isset($handler['settings_callback']) || isset($handler['terms_of_service'])) {
      $default_values = isset($settings['handler_settings'][$handler_id]) ? $settings['handler_settings'][$handler_id] : array();
      $form['handler_settings'][$handler_id] = array();
      $form['handler_settings'][$handler_id]['#type'] = 'fieldset';
      $form['handler_settings'][$handler_id]['#attributes'] = array('class' => array('geocoder-handler-setting', 'geocoder-handler-setting-' . $handler_id));
      $form['handler_settings'][$handler_id]['#title'] = $handler['title'] . ' Settings';
      $form['handler_settings'][$handler_id]['#states'] = array(
        'visible' => array(
          ':input[id="edit-instance-widget-settings-geocoder-handler"]' => array('value' => $handler_id),
        ),
      );

      if (isset($handler['terms_of_service'])) {
        $form['handler_settings'][$handler_id]['tos'] = array(
          '#type' => 'item',
          '#markup' => t('This handler has terms of service. Click the following link to learn more.') . ' ' . l($handler['terms_of_service'], $handler['terms_of_service']),
        );
      }

      if (isset($handler['settings_callback'])) {
        $settings_callback = $handler['settings_callback'];
        $form['handler_settings'][$handler_id] = array_merge($form['handler_settings'][$handler_id], $settings_callback($default_values));
      }
    }
  }
*/
/*
  $form['delta_handling'] = array(
    '#type' => 'select',
    '#title' => t('Multi-value input handling'),
    '#description' => t('Should geometries from multiple inputs be: <ul><li>Matched with each input (e.g. One POINT for each address field)</li><li>Aggregated into a single MULTIPOINT geofield (e.g. One MULTIPOINT polygon from multiple address fields)</li><li>Broken up into multiple geometries (e.g. One MULTIPOINT to multiple POINTs.)</li></ul>'),
    '#default_value' => isset($settings['delta_handling']) ? $settings['delta_handling']: 'default',
    '#options' => array(
      'default' => 'Match Multiples (default)',
      'm_to_s' =>  'Multiple to Single',
      's_to_m' =>  'Single to Multiple',
      'c_to_s' =>  'Concatenate to Single',
      'c_to_m' =>  'Concatenate to Multiple',
    ),
    '#required' => TRUE,
  );
  */
  return $form;
}

/**
 * Implements hook_field_attach_presave().
 *
 * Geocoding for the geocoder widget is done here to ensure that only validated
 * and fully processed fields values are accessed.
 */
function exif_date_field_attach_presave($entity_type, $entity) {
  // Loop over any datestamp field using our exif date widget
  $entity_info = entity_get_info($entity_type);
  $bundle_name = empty($entity_info['entity keys']['bundle']) ? $entity_type : $entity->{$entity_info['entity keys']['bundle']};
  foreach (field_info_instances($entity_type, $bundle_name) as $field_instance) {
    if ($field_instance['widget']['type'] === 'exif_date') {
      if (($field_value[LANGUAGE_NONE][0]['value'] = exif_date_widget_get_field_value($entity_type, $field_instance, $entity)) !== FALSE) {
        $entity->{$field_instance['field_name']} = $field_value;
      }
    }
  }
}

/**
 * Get a field's value based on geocoded data.
 *
 * @param $entity_type
 *   Type of entity
 * @para field_instance
 *   Field instance definition array
 * @param $entity
 *  Optionally, the entity. You must pass either the entity or $source_field_values
 * @param $source_field_values
 *  Array of deltas / source field values. You must pass either this or $entity.
 *
 * @return
 *  Three possibilities could be returned by this function:
 *    - FALSE: do nothing.
 *    - An empty array: use it to unset the existing field value.
 *    - A populated array: assign a new field value.
 */
function exif_date_widget_get_field_value($entity_type, $field_instance, $entity = NULL, $source_field_values = NULL) {
  if (!$source_field_values && !$entity) {
    trigger_error('exif_date_widget_get_field_value: You must pass either $source_field_values OR $entity', E_USER_ERROR);
    return FALSE;
  }

  $entity_info = entity_get_info($entity_type);

  // Required settings
  if (isset($field_instance['widget']['settings']['exif_date_field'])) {
    // $handler = geocoder_get_handler($field_instance['widget']['settings']['geocoder_handler']);
    $field_name = is_array($field_instance['widget']['settings']['exif_date_field']) ? reset($field_instance['widget']['settings']['exif_date_field']) : $field_instance['widget']['settings']['exif_date_field'];
    $target_info = field_info_field($field_instance['field_name']);
    $target_type = $target_info['type'];

    // Determine the source type, if it's a entity-key, we mock it as a "text" field
    if (in_array($field_name, $entity_info['entity keys']) && $entity) {
      $field_info = array('type' => 'text', 'entity_key' => TRUE);
    }
    else {
      $field_info = field_info_field($field_name);
      $field_info['entity_key'] = FALSE;
    }

    // Get the source values
    if (!$source_field_values) {
      if ($field_info['entity_key'] && $entity) {
        $source_field_values = array(array('value' => $entity->$field_name));
      }
      else if ($entity) {
          $source_field_values = field_get_items($entity_type, $entity, $field_name, isset($entity->language) ? $entity->language : NULL);
        }
        else {
          // We can't find the source values
          return FALSE;
      }
    }

    // Remove source values that are not valid.
    if ($source_field_values) {
      foreach ($source_field_values as $delta => $item) {
        if (!is_numeric($delta)) {
          unset($source_field_values[$delta]);
        }
      }
    }
    // If no valid source values were passed.
    if (empty($source_field_values)) {
      return FALSE;
    }

    // For entities being updated, determine if another geocode is necessary
    if ($entity) {
      if (!empty($entity->original)) {
       // dpm($entity, "entity has original");
        //@@TODO: Deal with entity-properties (non-fields)
        //@@TODO: This isn't working with file fields. Should use some kind of lookup / map
        $field_original = field_get_items($entity_type, $entity->original, $field_name, isset($entity->original->language) ? $entity->original->language : NULL);
        if (!empty($field_original)) {
          $diff = exif_date_widget_array_recursive_diff($field_original, $source_field_values);
          if (empty($diff)) {
            return FALSE;
          }
        }
      }
    }

    if (is_array($source_field_values) && count($source_field_values)) {
      $source = reset($source_field_values);
      // Geocode any value from our source field.
      if (isset($source['fid'])) {
        try {
          $date = exif_date_get_date($source['fid']);
        }
        catch (Exception $e) {
          watchdog_exception('exif date', $e);
          return FALSE;
        }
        return $date;
      }
    }
  }
}


function exif_date_widget_array_recursive_diff($aArray1, $aArray2) {
  $aReturn = array();
  if (empty($aArray1)) {
    return $aReturn;
  }
  foreach ($aArray1 as $mKey => $mValue) {
    if (array_key_exists($mKey, $aArray2)) {
      if (is_array($mValue)) {
        $aRecursiveDiff = exif_date_widget_array_recursive_diff($mValue, $aArray2[$mKey]);
        if (count($aRecursiveDiff)) { $aReturn[$mKey] = $aRecursiveDiff; }
      } else {
        if ($mValue != $aArray2[$mKey]) {
          $aReturn[$mKey] = $mValue;
        }
      }
    } else {
      $aReturn[$mKey] = $mValue;
    }
  }
  return $aReturn;
}
